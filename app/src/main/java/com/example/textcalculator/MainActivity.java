package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText firstOperand;
    EditText secondOperand;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstOperand = (EditText) findViewById(R.id.editText);
        secondOperand = (EditText) findViewById(R.id.editText2);
    }

    public void AddFirstAndSecondOperand(View view){
        int num1, num2, result;
        try {
            num1 = Integer.parseInt(firstOperand.getText().toString());
            num2 = Integer.parseInt(secondOperand.getText().toString());
            result = num1 + num2;
            setContentView(R.layout.activity_second);
            TextView textView = findViewById(R.id.textView3);
            textView.setText(num1 + " + " + num2 + " = " + result);
            if (num2 < 0) {
                textView.setText(num1 + " + ( " + num2 + ") = " + result);
            }
        }catch (NumberFormatException E){
            setContentView(R.layout.activity_second);
            TextView textView = findViewById(R.id.textView3);
            textView.setText("Неверный формат данных!");
        }
    }


}

